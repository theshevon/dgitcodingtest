package app;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that stores and enforces business rules.
 * @author Shevon Mendis, July 2020.
 */
public class ValidationEngine{

    private List<BusinessRule<?>> rules = new ArrayList<>();

    /**
     * Add's a new business rule the list of rules currently being enforced.
     * @param rule A new business rule.
     */
    public void addRule(BusinessRule<?> rule){
        this.rules.add(rule);
    }

    /**
     * Removes all the business rules that have been stored for use.
     */
    public void clearAllRules(){
        this.rules.clear();
    }

    /**
     * Runs all the business rules on the input argument. In addition, logs
     * the details of each rule being enforced and the result of its
     * enforcement.
     * @param words A list of words.
     */
    public void runAllRules(List<String> words){

        for (BusinessRule<?> rule : this.rules){
            System.out.println(">");
            System.out.println("> Rule: " + rule.getRuleName());
            System.out.println("> Result: " + rule.runRule(words));
            System.out.println(">");
        }
    }

}