package app;

import java.util.List;

/**
 * Interface that allows a business rule to be enforced.
 * @author Shevon Mendis, July 2020.
 */
public interface IRule<T> {

    /**
     * Enforces a business rule.
     * @param words A list of words.
     */
    public T runRule(List<String> words);

}