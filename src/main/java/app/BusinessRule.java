package app;

/**
 * Class that serves as a blueprint for all business rules.
 * @author Shevon Mendis, July 2020.
 */
public abstract class BusinessRule<T> implements IRule<T> {

    private String ruleName;

    public BusinessRule(String ruleName){
        this.ruleName = ruleName;
    }

    /**
     * @return A brief desciption of the rule's purpose.
     */
    public String getRuleName(){
        return this.ruleName;
    }

}