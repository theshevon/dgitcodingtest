package app;

import java.util.List;

/**
 * Class that represents a business rule that is intended to count the number
 * of words that have a specific character at a specific position.
 * @author Shevon Mendis, July 2020.
 */
public class UpperCaseValidator extends BusinessRule<Integer> {

    private final int charIndex;

    public UpperCaseValidator(int charIndex){

        super(String.format("Counts the number of words that have " +
            "a capitalised letter at index %d.", charIndex));

        this.charIndex = charIndex;
    }

    /**
     * @param words A list of words.
     * @return The number of words that have a capitalised letter at a
     * specific position.
     */
    @Override
    public Integer runRule(List<String> words) {

        int numWords = 0;

        for (String word : words){

            if (word.length() > this.charIndex &&
                Character.isUpperCase(word.charAt(this.charIndex))){
                numWords += 1;
            }

        }

        return numWords;
    }

}