package app;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Class that represents a business rule that is intended to extract all the
 * words that have a length greater than a specified threshold.
 * @author Shevon Mendis, July 2020.
 */
public class LengthThresholdValidator extends BusinessRule<List<String>> {

    private final int minLengthThreshold;

    public LengthThresholdValidator(int minLengthThreshold){
        super(String.format("Extracts words that have more than " +
                                "%d characters.", minLengthThreshold));

        this.minLengthThreshold = minLengthThreshold;
    }

    /**
     * @param words A list of words.
     * @return A list of words that have a length greater than a specified
     * threshold.
     */
	@Override
	public List<String> runRule(List<String> words) {
		return words.stream()
                    .filter(word -> word.length() > this.minLengthThreshold)
                    .collect(Collectors.toList());
	}

}