package app;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class that is used to parse the input files and create a list of words.
 *
 * @author Shevon Mendis, July 2020.
 */
public class InputParser {

    /**
     * Reads through a specified file and extracts all the words in it. A `word`
     * here is characterised as a group of characters delimited by white spaces.
     *
     * @param filepath Path to the (text) file containing the list of words.
     * @return A list of words (including duplicates).
     * @throws IOException
     */
    public static List<String> extractWordsFromFile(String filepath) throws IOException {

        List<String> words = new ArrayList<String>();

        BufferedReader reader = new BufferedReader(new FileReader(filepath));

        String line;
        while ((line= reader.readLine()) != null) {
            words.addAll(Arrays.asList(line.split("\\s+")));
        }

        reader.close();

        return words;
    }
}
