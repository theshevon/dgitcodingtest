package app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that serves as the entry point for the application.
 * @author Shevon Mendis, July 2020.
 */
public class Driver {

    // params for rule 1 (as specified in the requirements)
    public static int MIN_WORD_LENGTH = 5;

    // params for rule 2 (as specified in the requirements)
    public static int CHAR_INDEX = 0;

    public static void main(String[] args) throws Exception {

        if (args.length == 0){
            throw new Exception("Please include filenames as arguments!");
        }

        // run the business rules on the specified files
        for (String filepath: args){

            // extract list of words from file
            List<String> words = new ArrayList<>();
            try{
                words = InputParser.extractWordsFromFile(filepath);
            } catch (IOException e){
                e.printStackTrace();
                continue;
            }

            // exit if no words were found
            if (words.size() == 0){
                System.out.println("Error: No words could be extracted from this file!");
                continue;
            }

            System.out.println(">>> Filepath: " + filepath);

            // set up validation engine
            ValidationEngine validationEngine = new ValidationEngine();

            // -- add rules
            validationEngine.addRule(new LengthThresholdValidator(MIN_WORD_LENGTH));
            validationEngine.addRule(new UpperCaseValidator(CHAR_INDEX));

            // enforce rules
            validationEngine.runAllRules(words);
        }
    }
}