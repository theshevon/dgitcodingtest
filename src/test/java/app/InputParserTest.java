package app;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class used to test the Input Parser, which reads files and extracts the list
 * of words.
 *
 * @author Shevon Mendis, July 2020.
 */
class InputParserTest {

    @Test
    void testReadingEmptyFile() {
        List<String> expected = new ArrayList<>();
        List<String> actual;
        try {
            actual = InputParser.extractWordsFromFile("./src/res/text1.txt");
            assertEquals(expected, actual);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testReadingSingleLineFile() {
        List<String> expected = Arrays.asList("If", "you're", "visiting", "this", "page,", "you're", "likely", "here", "because", "you're", "searching", "for", "a", "random", "sentence.");
        List<String> actual;
        try {
            actual = InputParser.extractWordsFromFile("./src/res/text2.txt");
            assertEquals(expected, actual);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testReadingMultiLineFile() {
        List<String> expected = Arrays.asList("You", "have", "every", "right", "to", "be", "angry", "but", "that", "doesn't", "give", "you", "the", "right", "to", "be", "mean.");
        List<String> actual;
        try {
            actual = InputParser.extractWordsFromFile("./src/res/text3.txt");
            assertEquals(expected, actual);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}