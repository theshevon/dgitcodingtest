package app;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class used to test the Length Threshold business rule.
 * @author Shevon Mendis, July 2020.
 */
public class LengthThresholdValidatorTest {

    @Test
    void testEmptyList() {

        LengthThresholdValidator validator = new LengthThresholdValidator(10);

        List<String> expected = new ArrayList<>();
        List<String> actual = validator.runRule(new ArrayList<>());

		assertEquals(expected, actual);
    }

    @Test
    void testWordExceedingLengthThreshold() {

        LengthThresholdValidator validator = new LengthThresholdValidator(2);

        List<String> expected = Arrays.asList("hummus");
        List<String> actual = validator.runRule(Arrays.asList("hummus"));

		assertEquals(expected, actual);
    }

    @Test
    void testWordBelowLengthThreshold() {

        LengthThresholdValidator validator = new LengthThresholdValidator(5);

        List<String> expected = new ArrayList<>();
        List<String> actual = validator.runRule(Arrays.asList("boop"));

		assertEquals(expected, actual);
    }

    @Test
    void testWordMatchingLengthThreshold() {

        LengthThresholdValidator validator = new LengthThresholdValidator(5);

        List<String> expected = new ArrayList<>();
        List<String> actual = validator.runRule(Arrays.asList("jazzy"));

		assertEquals(expected, actual);
    }
}