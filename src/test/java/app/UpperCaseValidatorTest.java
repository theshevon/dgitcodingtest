package app;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class used to test the Character Presence business rule.
 * @author Shevon Mendis, July 2020.
 */
public class UpperCaseValidatorTest {

    @Test
    void testEmptyList() {

        UpperCaseValidator validator = new UpperCaseValidator(0);

        int expected = 0;
        int actual = validator.runRule(new ArrayList<>());

		assertEquals(expected, actual);
    }

    @Test
    void testRequiredIndexLowerThanWordLength() {

        UpperCaseValidator validator = new UpperCaseValidator(5);

        int expected = 0;
        int actual = validator.runRule(Arrays.asList("mmmmmmm"));

		assertEquals(expected, actual);
    }

    @Test
    void testRequiredIndexEqualToWordLength() {

        UpperCaseValidator validator = new UpperCaseValidator(5);

        int expected = 0;
        int actual = validator.runRule(Arrays.asList("mmmmm"));

		assertEquals(expected, actual);
    }

    @Test
    void testRequiredIndexHigherThanWordLength() {

        UpperCaseValidator validator = new UpperCaseValidator(5);

        int expected = 0;
        int actual = validator.runRule(Arrays.asList("mmmm"));

		assertEquals(expected, actual);
    }

    @Test
    void testCapitalisationAtStartIndex() {

        UpperCaseValidator validator = new UpperCaseValidator(0);

        int expected = 3;
        int actual = validator.runRule(Arrays.asList("&%Hi", "Hello", "there", "General", "Kenobi"));

        assertEquals(expected, actual);
    }

    @Test
    void testCapitalisationAtArbitraryIndex() {

        UpperCaseValidator validator = new UpperCaseValidator(4);

        int expected = 2;
        int actual = validator.runRule(Arrays.asList("You", "have", "everY", "right", "to", "be", "angry", "but", "that", "doesN't", "give", "you", "the", "right", "to", "be", "mean."));

        assertEquals(expected, actual);
    }

    @Test
    void testCapitalisationAtEndIndex() {

        UpperCaseValidator validator = new UpperCaseValidator(4);

        int expected = 1;
        int actual = validator.runRule(Arrays.asList("drinK", "drove", "mean."));

        assertEquals(expected, actual);
    }
}