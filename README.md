## Dgit Coding Test

#### Solution Details:

Assumptions made:

- A `word` is a group of characters that have been delimited by whitespace characters.
Hence, punctutation has not been stripped off of any word.
- As there was no mention in regards to using the return values from enforcing
the business rules, this solution prints them out.

Usage:

- The entry point for this application is `Driver.js`, which is located at
`./src/main/java/app`
- Filenames must be specified as commandline arguments. (If running on
VSCode, you can add filenames to the `args` field on line 12 in `.vscode/launch.json`)